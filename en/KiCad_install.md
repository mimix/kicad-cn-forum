## [KiCad 简介](./KiCad_description.md)

## [KiCad 安装](./KiCad_install.md)

### KiCad 下载

#### KiCad 国内镜像

1. [清华大学 TUNA 协会](https://mirrors.tuna.tsinghua.edu.cn/kicad/) 短链接为：http://tuna.kicad.cn（推荐👍）
2. [莞工 GNU/Linux 协会](https://mirrors.dgut.edu.cn/kicad/) 短链接为：http://dglinux.kicad.cn 开源软件镜像站
3. [南京大学开源镜像站](https://mirrors.nju.edu.cn/kicad/) 短链接为：http://nju.kicad.cn （电信路线速度慢）

#### KiCad 国外镜像：
1. [CERN 镜像](https://kicad-downloads.s3.cern.ch/index.html) （因为某些原因有时会出现无法访问）
2. [Github Releases](https://github.com/KiCad/kicad-winbuilder/releases) （因为某些原因有时会出现无法访问）


### KiCad 镜像目录使用说明（此处使用 TUNA 镜像为例说明，其余镜像同样适用）

#### [KiCad 镜像目录说明](https://mirrors.tuna.tsinghua.edu.cn/kicad/)

目录	        |	说明    |    备注
-----------|----------|---------
appimage/    |appimage 安装包|chmod +x \*.AppImage && ./*.AppImage
archive/    |压缩文件| 旧文件
docs/    |发行版的文档手册|包含 HTML、PDF、EPUB
doxygen/    |KiCad 开发配置|详细开发手册
doxygen-python/    |KiCad Python 开发配置|详细开发手册
libraries/    |封装库和集成库|发行版本的封装库和集成库
osx/    | KiCad OSX 版本目录    | 包含稳定版、夜间版和测试版
windows/    | KiCad Windows 版本目录    | 包含稳定版、夜间版和测试版
cleanup.sh    | 清理脚本    |无
favicon.ico    |图标    |无
list.js |JS 脚本    |无


#### [Windows 下载安装](http://kicad-pcb.org/download/windows/)

[KiCad Windows 版本下载](https://mirrors.tuna.tsinghua.edu.cn/kicad/windows/)

目录	        |	说明    |    备注
-----------|----------|---------
nightly/	| 夜间构建版本|无
stable/		| 发行版本|看版本号选择合适的下载
testing/	| 测试版本|选择对应分支下载测试

#### [OS X 下载安装](http://kicad-pcb.org/download/osx/)

[OS X 版本下载](https://mirrors.tuna.tsinghua.edu.cn/kicad/osx/)

目录	        |	说明    |    备注
-----------|----------|---------
nightly/	| 夜间构建版本|无
stable/		| 发行版本|看版本号选择合适的下载
testing/	| 测试版本|选择对应分支下载测试

#### Linux 下载安装

##### [AUR 包](http://kicad-pcb.org/download/arch-linux/)

添加 [ArchlinuxCN](https://mirrors.tuna.tsinghua.edu.cn/help/archlinuxcn/) 镜像:

```
yaourt -Syu
yaourt -S kicad-git
```

ArchlinuxCN 上的 kicad-git 实际上包含 kicad-git.git 和 kicad-i18n.git。kicad-i18n 能获得最新的翻译文件。

##### [Ubuntu](http://kicad-pcb.org/download/ubuntu/)

```
sudo add-apt-repository --yes ppa:js-reynaud/kicad-5.1
sudo apt update
sudo apt install --install-suggests kicad
sudo apt install kicad
```

##### [Debian](http://kicad-pcb.org/download/debian/)

```
sudo apt update
sudo apt install kicad
apt search kicad-doc
# or alternatively if you are on wheezy
apt-cache search kicad-doc
```

Debian 编译源码：

```
sudo apt install cmake doxygen libboost-context-dev libboost-dev \
libboost-system-dev libboost-test-dev libcairo2-dev libcurl4-openssl-dev \
libgl1-mesa-dev libglew-dev libglm-dev libngspice-dev liboce-foundation-dev \
liboce-ocaf-dev libssl-dev libwxbase3.0-dev libwxgtk3.0-dev python-dev \
python-wxgtk3.0-dev swig wx-common
```

##### [Linux Mint](http://kicad-pcb.org/download/linux-mint/)

```
sudo add-apt-repository --yes ppa:js-reynaud/kicad-5.1
sudo apt update
sudo apt install --install-suggests kicad
sudo apt install kicad
```

##### [Fedora](http://kicad-pcb.org/download/fedora/)

安装发行版：

```
dnf install kicad
dnf --enablerepo=updates-testing install kicad
dnf install kicad-packages3d
```

安装夜间版：

```
dnf copr enable @kicad/kicad
dnf install kicad
dnf install kicad-packages3d
dnf copr remove mangelajo/kicad
dnf install dnf-plugins-core
```

##### [OpenSUSE](http://kicad-pcb.org/download/open-suse/)

##### [Flatpak](http://kicad-pcb.org/download/flatpak/)

```
flatpak install --from https://flathub.org/repo/appstream/org.kicad_pcb.KiCad.flatpakref
```

##### [GNU Guix](http://kicad-pcb.org/download/gnu-guix/)

```
guix package -i kicad
```

##### [Gentoo](http://kicad-pcb.org/download/gentoo/)

```
emerge sci-electronics/kicad
```

##### [Sabayon](http://kicad-pcb.org/download/sabayon/)

```
equo install kicad
```

###### [源码安装](http://kicad-pcb.org/download/source/)


## [KiCad 文档](./KiCad_doc.md)