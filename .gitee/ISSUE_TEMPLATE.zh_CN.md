### 1. 现象描述

[描述问题的现象，最好能够提供屏幕截图 或 gif 动画]

### 2. 环境信息

[在此填写操作系统名称+版本等与问题相关的环境信息]

```
[KiCad 主界面] -> [帮助] -> [复制版本信息]
```

### 3. 重现步骤

[在此填写能够重现该问题的操作步骤]

- 步骤 1，...
- 步骤 2，...
- 步骤 3，...

### 4. 报错信息

[在此填写浏览器控制台中的报错信息、KiCad 的报错信息，如果没有报错信息可不填]


