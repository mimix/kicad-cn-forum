@[toc]
## [KiCad 插件](./KiCad_plugin.md)

## [KiCad 中国群文件](./KiCad_CN_group_files.md)

## 说明

- 本仓库使用 `git submodule` 方式储存 KiCad 中国群文件
- 使用 `工蜂` Git 服务，下载仓库需要登录腾讯云帐号（使用微信或 QQ 登录后修改用户名和密码，等会初始化子仓库需要使用腾讯云帐号和密码）。
- 如果在登录过程中出现用户名和密码输入错误，请使用下面 `Git` 命令行清除已登录的信息：

```bash
git config --system --unset credential.helper
```

## 教程

使用子模块仓库

```bash
// 初始化子模块仓库
git submodule init 

// 更新子模块仓库
git submodule update
```

或：

```bash
// 初始化和更新子模块仓库组合命令
git submodule update --init --recursive
```

从子模块的远端仓库更新并合并到本仓库

```bash
// 从子模块远端仓库更新并合并
git submodule update --remote --merge
```

# KiCad_CN 文件列表

```bash
├── KiCad_742339528.png
├── KiCad EDA
│  ├── Eeschema(汉化).pdf
│  ├── kicad-i18n_master_v0.18.zip
│  ├── KiCad_doc_zh_CN_v0.5.1.zip
│  ├── KiCad_i18n_manster_zh_CN_v0.13.zip
│  ├── KiCad_i18n_master_zh_CN_v0.0.12.rar
│  ├── KiCad_i18n_master_zh_CN_v0.0.23.tar.gz
│  ├── KiCad_i18n_master_zh_CN_v0.0.25.tar.gz
│  ├── KiCad_v5.1.5_3_i18n_zh_CN.rar
│  ├── KiCad_zh_CN_Master_v0.0.15.zip
│  ├── KiCad_zh_CN_Master_v0.0.16.7z
│  ├── KiCad_zh_CN_Master_v0.0.17.zip
│  ├── KiCad_zh_CN_Master_v0.0.24.zip
│  ├── KiCad_zh_CN_v5.1.5-3_v0.5.rar
│  ├── KiCad_zh_CN_v5.1.6_v0.1.7z
│  ├── KiCad_zh_CN_v5.1.6_v0.2.zip
│  ├── KiCad Windows 版本编译说明_v0.1.pdf
│  ├── KiCad入门（汉化）.pdf
│  ├── Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.appxbundle
│  ├── newstroke-font(KiCad 笔画字体工程).tgz
│  └── 中文文案排版指北.pdf
├── KiCad Python 插件
│  ├── altium2kicad-master (6).zip
│  ├── CircularZone.rar
│  ├── InteractiveHtmlBom-master动态Bom.zip
│  ├── kicad-round-tracks-master.zip
│  ├── kicad_scripts-master泪滴插件.zip
│  ├── spiki-master.zip
│  ├── toolbox-master.zip
│  └── WireIt-master.zip
├── KiCad 九分钟速成视频教程
│  ├── 1-KiCad-9分钟学会绘制原理图.mp4
│  ├── 2-KiCad-9分钟学会绘制PCB.mp4
│  ├── 3-KiCad-9分钟学会出Gerber文件.mp4
│  ├── 4-KiCad-9分钟学会安装使用插件.mp4
│  ├── 5-KiCad-9分钟学会编辑符号库.mp4
│  ├── 6-Kicad-9分钟学会封装向导.mp4
│  ├── 7-kiCad-9分钟学会手工创建封装.mp4
│  ├── 8-KiCad-9分钟学会弧线和微带线过孔.mp4
│  ├── 9-KiCad-9分钟学会蛇形等长布线.mp4
│  ├── 10-KiCad-9分钟学会层次图与设计规则.mp4
│  ├── 11-KiCad-9分钟学会圆形覆铜+禁止覆铜+批量地过孔.mp4
│  ├── 12-KiCad-9分钟学会封装自动排列和批量选择与编辑.mp4
│  └── README.md
├── LC2KiCad
│  ├── demo视频
│  │  └── LC2KiCad导出PCB中所有库.mp4
│  └── 发布版本
│     ├── lc2kicad-alpha-0.1.4-mingw-x64.exe
│     ├── lc2kicad-alpha-0.1.4-msvc-x64.exe
│     ├── lc2kicad-alpha-0.1.4-msvc-x86.exe
│     ├── lc2kicad-alpha-0.1.4-testing-1-mingw.exe
│     ├── lc2kicad-alpha-0.1.31-mingw.exe
│     ├── lc2kicad-alpha-0.1.41-mingw-x64.exe
│     ├── lc2kicad-alpha-0.1.41-msvc2019-x64.exe
│     ├── lc2kicad-alpha-0.1.41-msvc2019-x86.exe
│     └── lc2kicad-alpha-0.15-mingw32.exe
├── LICENSE
├── msk
│  ├── msk-002.zip
│  ├── msk-003.zip
│  ├── msk-006.zip
│  ├── msk-007.zip
│  ├── msk-007_leapfrog_gerber.zip
│  ├── msk-008.zip
│  ├── msk-009.zip
│  ├── msk-010.zip
│  ├── msk-011.zip
│  └── msk-012.zip
├── PCB 设计生产规范
│  ├── collection-2013-2014_cn.pdf
│  ├── collection-2014-2015_cn.pdf
│  ├── collection-2015-2016_cn.pdf
│  ├── compilation-apm-solutions-6_cn.pdf
│  ├── eetop.cn_icc2dp.pdf
│  ├── GBPCB行业规则.pdf
│  ├── RS-485保护电路大全2018.pdf
│  ├── 【选型】Rogers 高频印刷线路板材料选型指南.pdf
│  ├── 华为PCB设计规范.pdf
│  ├── 印制电路板行业规范公告管理暂行办法.pdf
│  ├── 印制电路板行业规范条件.pdf
│  └── 基础电子元器件的特性、选型及应用.pdf
├── Python  官方中文手册
│  ├── python-2.7.17-docs-pdf-a4.zip
│  ├── python-3.7.6-docs-pdf-a4.zip
│  └── python-3.8.1-docs-pdf-a4.zip
├── README.md
├── RF 天线设计
│  ├── 001-96160_AN91445_-_Antenna_Design_and_RF_Layout_Guidelines_Chinese.pdf
│  ├── 002-00012_AN91184_PSoC_4_BLE_-_Designing_BLE_Applications_Chinese.pdf
│  ├── 002-25056_AN218241_PSoC_6_MCU_Hardware_Design_Considerations_Chinese.pdf
│  └── AN79938(ZH)_001-96370_00_V.pdf
├── Tigard
│  ├── tigard-9pcs-bottom.jpg
│  ├── tigard-9pcs-ibom.html
│  ├── tigard-9pcs-top.jpg
│  ├── tigard-9pcs.zip
│  ├── tigard-9pcs_BOM.csv
│  ├── tigard-9pcs_gerber.zip
│  └── tigard-9pcs_POS.csv
├── TODO
│  ├── 2.0 kicad - 快捷键.jpg
│  ├── 3-6.1-2.5.kicad_mod
│  ├── 3x6x3.5_SW_side.kicad_mod
│  ├── 9a2e3fc800b79cb4324845e4cafe7ec837561d64.gif
│  ├── 87fce4d47936f381ba62de244aaf917.jpg
│  ├── 111.jpg
│  ├── 2020-03-12 11-58-02 的屏幕截图.png
│  ├── 2020-03-12 11-58-56 的屏幕截图.png
│  ├── 2020-03-12 16-51-34 的屏幕截图.png
│  ├── 2020-03-12 16-54-00 的屏幕截图.png
│  ├── 2020-04-22_13-48.png
│  ├── 2020-04-25 17-56-02屏幕截图.png
│  ├── 11074.zip
│  ├── 1923885.pdf
│  ├── 632723300011.stp
│  ├── Adafruit-Bluefruit-LE-UART-Friend-PCB-master.zip
│  ├── add03be79b08e4c17b32cb40b31740f7a185ea55(1).gif
│  ├── add03be79b08e4c17b32cb40b31740f7a185ea55.gif
│  ├── Air202.kicad_mod
│  ├── air202.stp
│  ├── AIR202 LUAT.STEP
│  ├── AIR268.kicad_mod
│  ├── AIR720.STEP
│  ├── AIR720D.kicad_mod
│  ├── Air720G.kicad_mod
│  ├── Air720G_Small.kicad_mod
│  ├── Air720SL.kicad_mod
│  ├── Air800.kicad_mod
│  ├── Animation.gif
│  ├── arduino936.zip
│  ├── balancingcar_borad.zip
│  ├── behave-dark.json
│  ├── BGA256C100P16X16_1700X1700X155..d
│  ├── C150929_IRS2301SPBF_2018-01-25.PDF
│  ├── CC2500.tar.gz
│  ├── ch552G +n76e003at20.rar
│  ├── cmf_commercial_signal_acm2012_zh.pdf
│  ├── Codelife.pdf
│  ├── coiler_gerber_1.zip
│  ├── coiler_gerber_2.zip
│  ├── com.jls.jlc.ui.apk
│  ├── D2-常用的网站链接.zip
│  ├── DAPLink.7z
│  ├── Demo of WireIt Tools for PCBNEW - YouTube.mp4
│  ├── DigtalPower.asc
│  ├── DigtalPower.pdf
│  ├── dvk-mx8m-bsb.pdf
│  ├── eda移动.mp4
│  ├── eeschema
│  ├── EL-E.pdf
│  ├── EL.pdf
│  ├── EmBitz_1.11.exe
│  ├── Fact_sheet_Mk3.pdf
│  ├── FBGA-256_3D.zip
│  ├── FE8.1-Data-Sheet-1.0.pdf
│  ├── ffp-F_Fab.pdf
│  ├── flexRoundingSuite-master.zip
│  ├── GB14048.1-2006.pdf
│  ├── GD32VF103_Datasheet_Rev1.0.pdf
│  ├── GD32VF103_Firmware_Library_V1.0.0.rar
│  ├── GD32VF103_User_Manual_CN_V1.0.pdf
│  ├── GD32VF103V_EVAL_Demo_Suites.rar
│  ├── gerber.rar
│  ├── GIF.gif
│  ├── Git零基础实战.pdf
│  ├── GX12.zip
│  ├── gz.gif
│  ├── HackingTheXbox_Free.pdf
│  ├── hosts
│  ├── IP178G_GH_Fiber_application_circuit_20130109.zip
│  ├── key.PcbLib
│  ├── kicad-cheatsheet-landscape.pdf
│  ├── kicad-color-schemes-master.zip
│  ├── kicad.7z
│  ├── KICAD.png
│  ├── KiCad_plugin.md
│  ├── kicad_tools-master(1).zip
│  ├── kicad_tools-master.zip
│  ├── kicadutil.jar
│  ├── kicad不缩放画布移动.mp4
│  ├── KiCAD 坐标格式整理.xlsm
│  ├── kicad插件.zip
│  ├── kicad移动.mp4
│  ├── KICAD背面.png
│  ├── lc_kicad_lib-master.zip
│  ├── lc_kicad_lib.zip
│  ├── leapfrog_1.zip
│  ├── leapfrog_gerber.zip
│  ├── ledtouch.wmv
│  ├── libzstd_jb51.rar
│  ├── Linux折腾笔记v6.0.1.pdf
│  ├── Linux程序设计第4版_图灵程序设计.epub
│  ├── LonganNano_sch.pdf
│  ├── LOPYOEM_Mother_Board_V1.0r_20170413.PcbDoc_output.txt
│  ├── mil与mm换算器.exe
│  ├── MobaXterm_Installer_v12.4.zip
│  ├── mt7232.pdf
│  ├── My_GSM_SIMCOM.lib
│  ├── N76E003_HC89S003.zip
│  ├── netease-cloud-music-1.2.1-2-x86_64.pkg.tar.xz
│  ├── NEW_PCB.json
│  ├── novena-accessories-kicad.zip
│  ├── novena-mechassy-pvt.stp.zip
│  ├── OpenSCAD-2019.05-x86-64-Installer.exe
│  ├── pcb.kicad_pcb
│  ├── PCB.pdf
│  ├── PCB3.DXF
│  ├── Pcbnew.jpg
│  ├── Pcbnew_position_relative_cartesian.png
│  ├── PCB设计与制造-应用教材.pdf
│  ├── PGEC_HexUnitModule_Back.jpg
│  ├── PGEC_HexUnitModule_Buttom.jpg
│  ├── PGEC_HexUnitModule_Front(1).jpg
│  ├── PGEC_HexUnitModule_Front.jpg
│  ├── pll_verification_ws_v1.0_001.tar.gz
│  ├── plugins.tmp
│  ├── project_stander_elec_v1.zip
│  ├── qcef-1.1.6-1-x86_64.pkg.tar.xz
│  ├── RDK FractN PLL Tutorial v1.0_090420.pdf
│  ├── RJ45_SINGAL_PORT.dxf
│  ├── RPI-CMIO-V3_0-PUBLIC.brd
│  ├── S00107-11560295.mp4
│  ├── S00107-12002595.mp4
│  ├── S00107-12215758.mp4
│  ├── SIM8060-6-0-14-00-A_revA.STEP
│  ├── SMA__EdgeMount.kicad_mod
│  ├── Snipaste_2020-02-01_21-37-25.png
│  ├── Snipaste_2020-02-01_21-37-36.png
│  ├── Snipaste_2020-02-04_11-20-07.png
│  ├── Snipaste_2020-02-04_11-20-18.png
│  ├── Spycheck-Windows-20200511.zip
│  ├── sqjq910el-1766673.pdf
│  ├── step-mxo2_硬件手册v1.1.pdf
│  ├── step-mxo2v2.2原理图.pdf
│  ├── stm32-1.pdf
│  ├── stm32-linux.pdf
│  ├── stm32-vserprog-gerber-v4.zip
│  ├── sw.kicad_mod
│  ├── SY7088.pdf
│  ├── sy8205ffc.zip
│  ├── T12-STM8S003(1).pdf
│  ├── T12-STM8S003.pdf
│  ├── teardrop_gui.py
│  ├── teardrops.zip
│  ├── TF_CARD.kicad_mod
│  ├── tours.gif
│  ├── transforms_zh.scad.md
│  ├── TSSOP-38_L9.7-W4.4-P0.50-LS6.4-BL.json
│  ├── TWRP-3.3.1-1029-XIAOMI8SE-CN-wzsx150-fastboot.7z
│  ├── TWRP-3.3.1-1029-XIAOMI8SE-CN-wzsx150.zip
│  ├── ubertooth-one.rar
│  ├── uconfig-win32-v0.zip
│  ├── USB-A-S-F-B-TH.step
│  ├── USB-AM-S-F-B-TH.step
│  ├── USB-B-S-F-B-TH.step
│  ├── USB.zip
│  ├── USB1061-GF-L-A.stp
│  ├── usb_a-usb_a-top-c.stp
│  ├── USB_Micro-B-5s.kicad_mod
│  ├── usbarmory.zip
│  ├── User Library-Micro_USB-1.STEP
│  ├── User Library-USB - C.step
│  ├── vca.eps
│  ├── VID_20200306_134335.mp4
│  ├── Video_2020-03-25_110433.wmv
│  ├── vim零基础入门.txt
│  ├── WPC_QI_5W_V3.0.pcb
│  ├── wpsfontv1.0.tar.gz
│  ├── wsn_v2.zip
│  ├── WVSN_5.7z
│  ├── XT系列.zip
│  ├── ZINGTO_IO_Hi3521D_FRONT.jpg
│  ├── 三菱PLC的485通讯.docx
│  ├── 上海贝岭产品列表.xlsx
│  ├── 元件列表.mp4
│  ├── 屏幕录制 2020-03-08 下午8.12.11.mov
│  ├── 屏幕录制2020-04-29上午8.32.45.mov
│  ├── 嵌入式C语言自我修养.pdf
│  ├── 开源安卓投屏工具scrcpy-win64-v1.12.1.zip
│  ├── 归档2.zip
│  ├── 提问的智慧+回答的智慧.txt
│  ├── 插件.zip
│  ├── 数控电源原理图(1).pdf
│  ├── 数控电源原理图.pdf
│  ├── 无标题.png
│  ├── 深度截图_20200514214911(1).png
│  ├── 深度截图_20200514214911.png
│  ├── 深度截图_选择区域_20190828200003(1).png
│  ├── 深度截图_选择区域_20190828200003.png
│  ├── 深度截图_选择区域_20191010132200.png
│  ├── 深度截图_选择区域_20191010132957.png
│  ├── 深度截图_选择区域_20191010172526.png
│  ├── 矽力杰产品列表.xlsx
│  └── 自动布线.exe
├── X-MagicBox
│  ├── 2.jpg
│  ├── 6B40F16820CDBFD4AAEFF21A64A5BDD2.mp4
│  ├── Air724.kicad_mod
│  ├── air724.step
│  ├── binding-post-4mm-speaker-terminal-1.snapshot.4.zip
│  ├── en.DM00235987.pdf
│  ├── jack-pj313d-1.snapshot.9.zip
│  ├── My_GSM_SIMCOM.lib
│  ├── My_RF_Modules.pretty.zip
│  ├── pj-307-stereo-connector-1.snapshot.5.zip
│  ├── PJ311 v1.step
│  ├── st-111-st-212-audio-connector-3-5-mm-1.snapshot.1.zip
│  ├── usb-type-c-port-smd-type-1.snapshot.3.zip
│  ├── xfmh.gif
│  ├── XiaoFanMoHe-cache.lib
│  ├── XiaoFanMoHe.pdf
│  ├── XiaoFanMoHe.pretty.zip
│  ├── xiaofanmohe.stp
│  ├── XiaoFanMoHe.wrl
│  ├── xiaofanmohe.zip
│  ├── XiaoFanMoHe_64.exe
│  └── xiaofanmohe_asm.stp
├── 【必看】KiCad 简体中文手册
│  ├── KiCad_doc_zh_CN_v0.4.4.tar(1).gz
│  ├── KiCad_doc_zh_CN_v0.4.4.tar.gz
│  ├── KiCad_doc_zh_CN_v0.5.1.zip
│  └── KiCad_zh_CN_v5.1.5-3_v0.6.zip
├── 【自动布线插件】 FreeRouting
│  ├── FreeRouting-cn.zip
│  ├── FreeRouting-master-source.zip
│  ├── freeRouting.jar
│  ├── Kicad-freerouting.jar
│  ├── layout-20190820-1-x86_64.pkg.tar.xz
│  └── layout_20190820_Ubuntu_19.04_amd64.deb
├── 新概念模拟电路
│  ├── 新概念模拟电路1-晶体管.pdf
│  ├── 新概念模拟电路2-负反馈和运算放大器基础.pdf
│  ├── 新概念模拟电路3-运算放大器的频率特性和滤波器.pdf
│  ├── 新概念模拟电路4-信号处理电路.pdf
│  └── 新概念模拟电路5-源电路（信号源和电源）.pdf
├── 电子书_Linux_Hack_嵌入式_Windows_PHP_Vim
│  ├── [Arduino从基础到实践].BEGINNING.ARDUINO.[美].Michael.McRoberts.著.杨继志，郭敬.译.pdf
│  ├── ARM 汇编语言官方手册（中文）.pdf
│  ├── C2Rust 手册.pdf
│  ├── Cortex M3权威指南(中文).pdf
│  ├── C Primer Plus 第6版 非扫描版 中文版.pdf
│  ├── C标准库（中文版）.pdf
│  ├── C 语言变成透视.pdf
│  ├── C语言深度解剖2010版全.pdf
│  ├── Deepin折腾笔记v6.4.pdf
│  ├── Electron v6.0 中文文档.pdf
│  ├── GCC 中文手册.pdf
│  ├── Google 开源项目风格指南 (中文版).pdf
│  ├── Kali Linux Web 渗透测试秘籍(1).pdf
│  ├── Kali Linux Web 渗透测试秘籍.pdf
│  ├── Kali Linux 秘籍.pdf
│  ├── LFS-BOOK(Linux From Scratch (简体中文版)版本 9.0).pdf
│  ├── LFS-SYSD-BOOK(Linux From Scratch (简体中文版)版本 9.0-systemd).pdf
│  ├── Linux C 编程一站式学习.pdf
│  ├── Linux Kernel 核心中文手册.chm
│  ├── Linux 内核文档（中文版）.pdf
│  ├── Linux 字体配置要略.pdf
│  ├── Linux工具快速教程.pdf
│  ├── Linux性能优化.pdf
│  ├── Linux性能调优指南.pdf
│  ├── Linux操作系统编译构建指南（内核 2.6.x 过时转看 LFS 构建）.pdf
│  ├── Linux程序设计第4版.epub
│  ├── Linux 诞生和发展的五个重要支柱.pdf
│  ├── Makefile 基础教程.pdf
│  ├── Markdown编辑器语法.md
│  ├── Nmap Reference Guide(Nmap参考指南).pdf
│  ├── PHPer 必知必会的 Linux 命令.pdf
│  ├── QEMU KVM学习笔记.pdf
│  ├── RustPrimer 中文版.pdf
│  ├── Rust 学习笔记.pdf
│  ├── Rust 宏小册 中文版.pdf
│  ├── Rust 死灵书 - Rust 高级与非安全程序设计.pdf
│  ├── rust 程序设计语言.pdf
│  ├── Rust 程序设计语言（第二版 & 2018 edition）简体中文版.epub
│  ├── Rust 程序设计语言（第二版 & 2018 edition）简体中文版.mobi
│  ├── Rust 程序设计语言（第二版 & 2018 edition）简体中文版.pdf
│  ├── Rust 语言学习笔记.pdf
│  ├── systemd 中文版.pdf
│  ├── TLP高级电源管理.doc
│  ├── Web Hacking 101.pdf
│  ├── Web开发者手边的一本CentOS小书.pdf
│  ├── Windows 批处理(bat)语法大全.pdf
│  ├── Windows 绝赞应用.pdf
│  ├── 中文文案排版指北.pdf
│  ├── 代码整洁之道.pdf
│  ├── 像 IDE 一样使用 vim.pdf
│  ├── 命令行的艺术.pdf
│  ├── 基础电子元器件的特性、选型及应用.pdf
│  ├── 大学霸 Kali Linux 安全渗透教程.pdf
│  ├── 完全用 GNU_Linux 工作.pdf
│  ├── 嵌入式 Linux 知识库.pdf
│  ├── 征服 Linux.pdf
│  ├── 操作系统思考.pdf
│  ├── 模拟工程师口袋参考书.pdf
│  ├── 窗口管理器 Openbox 入门指南.pdf
│  ├── 英语进阶指南.pdf
│  ├── 通过例子学 Rust.pdf
│  └── 黑客攻防技术宝典 浏览器实战篇.pdf
└── 精通 Git
   ├── git-community-book.pdf
   ├── Git 在线学习网站.txt
   ├── progit_v2.1.36.epub
   ├── progit_v2.1.36.mobi
   ├── progit_v2.1.36.pdf
   └── 精通 Git（Pro Git）.pdf


```
